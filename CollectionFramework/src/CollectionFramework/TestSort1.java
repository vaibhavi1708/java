
package CollectionFramework;
import java.util.*;

public class TestSort1 {
    public static void main(String args[]){  
        ArrayList<Student>al=new ArrayList<Student>();
        al.add(new Student(101,"Vijay",23));
        al.add(new Student(106,"Ajay",27));
        al.add(new Student(105,"Jay",21));
        
        Collections.sort(al);
        for(Student st:al)
        {
            System.out.println(st.rollno+" "+st.name+" "+st.age);
        }
        
        
    }
}
