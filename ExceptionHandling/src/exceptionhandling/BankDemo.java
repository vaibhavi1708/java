
package exceptionhandling;


public class BankDemo {
    public static void main(String[]args){
        CheckingAccount ck=new CheckingAccount(101);
        System.out.println("Depositing 500");
        ck.deposit(500);
        
        try{
            System.out.println("withdrawing 100");
            ck.withdraw(100);
            System.out.println("withdrawing 600");
            ck.withdraw(600);
        }catch(InsufficientFundsException e){
            System.out.println("you are short of : "+e.getAmount());
            e.printStackTrace();
        }
        
    }
    
}
