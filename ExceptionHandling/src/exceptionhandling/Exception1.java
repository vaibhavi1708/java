
package exceptionhandling;
import java.io.*;


public class Exception1 {

    
    public static void main(String[] args) {
        int a[] = new int[2];
       try {
         
         System.out.println("Access element three :" + a[3]);
      } catch (ArrayIndexOutOfBoundsException e) {
         System.out.println("Exception thrown  :" + e.getMessage());
      }finally
       {
           a[0]=6;
           System.out.println("Element at 0 index is : "+a[0]);
           
       }
      System.out.println("Out of the block");
   }
    
    
}
