
package exceptionhandling;


public class InsufficientFundsException extends Exception {
    private double amount;
    
    InsufficientFundsException(double amount)
    {
        this.amount=amount;
    }
    public double getAmount()
    {
        return amount;
    }
    
}
