
package newpackage;

import java.applet.Applet;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class NewApplet extends Applet implements ActionListener {
    
    List subjects;
    String str=" ";

    
    public void init() {
        setLayout(null);
        subjects=new List(4,true);
        subjects.add("mathematics");
        subjects.add("physics");
        subjects.add("chemistry");
        subjects.add("english");
        subjects.add("hindi");
        subjects.add("biology");
        subjects.add("science");
        subjects.add("zoology");
        
        
        subjects.setFont(new Font("Times New Roman",Font.BOLD,18));
        subjects.setBounds(100, 50, 250, 250);
        subjects.addActionListener(this);
        add(subjects);
        
    }
    public void actionPerformed(ActionEvent ar)
    {
        int arr[]=subjects.getSelectedIndexes();
        for(int c=0;c<arr.length;++c)
        {
            str=str+subjects.getItem(arr[c])+" , ";
            repaint();
        }
    }
    public void paint(Graphics gr)
    {
        gr.setFont(new Font("Times New Roman",Font.BOLD,18));
        gr.setColor(Color.red);
        gr.drawString(str, 10, 400);
    }

    
}
