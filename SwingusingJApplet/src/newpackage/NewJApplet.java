
package newpackage;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;


public class NewJApplet extends JApplet implements ActionListener{
    JLabel lb1;
    JPasswordField psf;
    JButton bt1;
    String str=" ";

    
    public void init() {
       Container ct=getContentPane();
       ct.setLayout(new FlowLayout());
       
       lb1=new JLabel("Enter the password");
       psf=new JPasswordField(25);
       bt1=new JButton("Submit");
       
       bt1.addActionListener(this);
       add(lb1);add(psf);add(bt1);
       
    }
    
    public void actionPerformed(ActionEvent er)
    {
       String val=er.getActionCommand();
       if(val.equals("Submit"))
       {
           if(psf.getText().equals("India"))
               str="You are an Authorized user";
           else
               str="You are an Unauthorized user";
       }
       JOptionPane.showMessageDialog(null,str);
               
    }
    
   /* public void paint(Graphics gr)
    {
        gr.setFont(new Font("Impact",Font.BOLD,18));
        gr.drawString(str, 20, 80);
    }*/

    
}
