package NewPackage;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class delete
 */
@WebServlet("/delete")
public class delete extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public delete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		response.setContentType("Text/html");
		PrintWriter out=response.getWriter();
		String code=request.getParameter("code");
		java.sql.Connection conn;
		PreparedStatement Pst;
		String query="delete from managerial where code = "+Integer.parseInt(code);
		int n=0;
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			conn= 
					DriverManager.getConnection("jdbc:mysql://localhost:3306/company?allowPublicKeyRetrieval=true&useSSL=false","root","root");
			Pst=conn.prepareStatement(query);
			n=Pst.executeUpdate();
			if(n==0)
			{
				out.print("<h1>No such record was found</h1>");
			}
		
		
		}catch(Exception er)
		{
		out.println(er);
		}
	}

}
