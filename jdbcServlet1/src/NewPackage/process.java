package NewPackage;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sun.jdi.connect.spi.Connection;

/**
 * Servlet implementation class process
 */
@WebServlet("/process")
public class process extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public process() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		response.setContentType("Text/html");
		PrintWriter out=response.getWriter();
		java.sql.Connection conn;
		PreparedStatement Pst;
		ResultSet rs;
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			conn= 
					DriverManager.getConnection("jdbc:mysql://localhost:3306/company?allowPublicKeyRetrieval=true&useSSL=false","root","root");
			Pst=conn.prepareStatement("select * from managerial");
			rs=Pst.executeQuery();
			out.print("<table border='1' align='center'>");
			while(rs.next())
			{
				out.print("<tr><th>"+rs.getInt(1)+"</th><th>"+rs.getString(2)+"</th><th>"+rs.getString(3)+"</th><th>"+rs.getDate(5)+"</th></tr>");
				
			}
			out.print("</table>");
			
			
		}
		catch(Exception er)
		{
			out.print(er);
		}
	
	}

}
